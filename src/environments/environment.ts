export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAa4iDCl0m8nNQyvPHIxXRKExeNqnDWR3k',
    authDomain: 'kine-run-16c38.firebaseapp.com',
    databaseURL: 'https://kine-run-16c38.firebaseio.com',
    projectId: 'kine-run-16c38',
    storageBucket: 'kine-run-16c38.appspot.com',
    messagingSenderId: '807077824834',
    appId: '1:807077824834:web:43845556989291b67f20db',
    measurementId: 'G-V4L1SK6W7Q',
  },
};
