import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss'],
})
export class GraphComponent {
  constructor(public db: AngularFirestore) {
    this.displayUsers();
  }
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  public barChartLabels = ['Vitesse moyenne (en kh)'];
  public barChartType = 'line';
  public barChartLegend = true;
  public barChartData = [
    {
      data: [],
      label: 'Vitesse moyenne',
    },
  ];

  public selected = 'bars';
  public users;
  public courseId;

  public async getCourse(user) {
    const courseId = await this.db
      .collection('users')
      .doc(user)
      .collection('runs')
      .get()
      .toPromise();
    return courseId;
  }

  public async user(user) {
    this.barChartData[0].data = [];
    if (this.db.collection('users').doc(user).collection('runs')) {
      const courseId = (await this.getCourse(user)).docs[0].id;
      this.db
        .collection('users')
        .doc(user)
        .collection('runs')
        .doc(courseId)
        .collection('positions')
        .get()
        .subscribe((res) => {
          res.docs.forEach((element) => {
            this.db
              .collection('users')
              .doc(user)
              .collection('runs')
              .doc(courseId)
              .collection('positions')
              .doc(element.id)
              .snapshotChanges()
              .subscribe((res) => {
                const datas: any = res.payload.data();
                this.barChartData[0].data.push(datas.coords.speed);
              });
          });
        });
    } else {
      console.log('No Course for this user !');
    }
  }

  public displayUsers() {
    this.users = this.db.collection('users').snapshotChanges();
  }
}
