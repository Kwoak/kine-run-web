import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
  public selected = 'line';
  public users;

  constructor(public db: AngularFirestore) {
    this.displayUsers();
  }

  displayUsers() {
    this.users = this.db.collection('users').snapshotChanges();
  }

  public deleteUser(user) {
    this.db.collection('users').doc(user).delete();
  }
}
