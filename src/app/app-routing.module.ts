import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GraphComponent } from './graph/graph.component';
import { UsersComponent } from './users/users.component';
import { FormComponent } from './form/form.component';

const routes: Routes = [
  { path: 'graph', component: GraphComponent },
  { path: 'users', component: UsersComponent },
  { path: 'login', component: FormComponent },
  { path: '**', component: FormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
