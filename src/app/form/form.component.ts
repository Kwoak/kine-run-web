import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  public email: string;
  public password: string;
  constructor(public auth: AngularFireAuth, private router: Router) {}

  ngOnInit(): void {}

  login() {
    this.auth
      .signInWithEmailAndPassword(this.email, this.password)
      .then(() =>
        this.router
          .navigate(['/', 'graph'])
          .catch((error) => console.log(error))
      );
  }
}
